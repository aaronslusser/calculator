//
//  CalculatorApp.swift
//  Calculator
//
//  Created by  Amber  on 11/15/21.
//

import SwiftUI

@main
struct CalculatorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
